/* Andrew Raddatz
 * CS 350 Lab 2 Key
 * Wednesday February 8th, 2012
 * */

/* Part 1
 *
 *  1)
 *      a) 9 bits are needed
 *      b) 112 more students could be added
 *
 *  2) 
 *      a)
 *       0111
 *      +1000
 *      =1111 = -0 = 0
 *      b)
 *       0111
 *      +1111
 *      =0110 = 6 != 0
 *      c)
 *       0111
 *      +1001
 *      =0000 = 0
 *
 *  3)  BED0 0000
 *      a) (11*16^8)+(14*16^7)+(13*16^6)+0 = 51 220 840 448
 *      b) 1011 1110 1101 0000 0000 0000 0000 0000
 *         1 01111101 101 0000 0000 0000 0000 0000 
 *           = -1.101 * 2^(125-127) = -.01101 = -13/32 = -0.40625
 *
 *  4)  5 43/64
 *      a) 101.101011      
 *     
 *  5)  5 43/64
 *      a) 0 10000001 01101011000000000000000
 *      a) 0100 0000 1011 0101 1000 0000 0000 0000= 40B5 8000
 *
 *  6) 11.11111 11111 11111 11111 111
 *      a) this has 25 bits, we can only store 24 of them in 32 bit IEEE
 *      b) 100 (4)
 *      c) 11.11111 11111 11111 11111 11 (24 bits)
 *  
 *  7) Truncation Error
 *      a)
 *       1.1111
 *      +0.11111
 *     =10.11101 -> 10.111  yes truncation error
 *
 *      b)
 *       .11111
 *      +.11111
 *     =1.11110 ->  1.1111   no truncation error
 *
 *      c)
 *       .11111
 *      +.11111
 *     =1.1111
 *     +1.1111
 *    =11.1110  -> 11.111   no truncation error
 *
 *      d)
 *       1.1111
 *      +0.11111
 *     =10.11101 - 10.111   yes truncation error
 *      
 *      no point in testing the next addition
 *
 *      e) floating point is not associative 
 * */

#include<stdio.h>
#include<string.h>
#include<math.h>

void partd(char *str);
int parte(char *str);
void partf(char *str);

int main(){
    int done = 0;

    while (!done){
        char userinput[80];
        // part a
        printf("Enter a string of 1's and 0's or enter q to exit: ");

        // part b
        scanf("%s",userinput);

        // part c
        if (strcmp(userinput,"q")==0){done = 1; break;}

        // part e
        if (parte(userinput)){continue;}

        // part d
        partd(userinput);

        // part f
        partf(userinput);
    }
}

void partd(char *str){
    int len = strlen(str), i, val=0;

//     adding everything up
    for (i = len; i>=0; i--){ if (str[i]==49){val = val + pow(2,len-1-i);}}
    printf("String Length: %d\nString Value: %d\n",len,val);
}

int parte(char *str){
    int len = strlen(str), i;
    for (i = 0; i<len; i++){ 
        if (str[i]>'1' || str[i]<'0'){
            printf("First Offending Character: %c\n",str[i]); 
            return 1;
        }
    }
        return 0;
}

void partf(char *str){
    int len = strlen(str), i;

//     1's comp
    for (i = 0; i<len; i++){ if (str[i]=='1'){str[i]='0';} else {str[i]='1';}}

//     creating a char array with a 1 at the end
    char one[len], ans[len], temp;
    for (i = 0; i<len-1; i++){ one[i] = 48;}
    one[len-1]=49;

//     manually doing addition
    for (i = len-1; i>=0; i--){
        temp = one[i]+str[i];
        if (temp=='0'+'0'){ ans[i]='0';}
        if (temp=='0'+'1'){ ans[i]='1';}
        if (temp=='1'+'1'){ str[i-1]++; ans[i]='0';}
        if (temp=='1'+'2'){ str[i-1]++; ans[i]='1';}
    }
    
//     terminating the string
    ans[len]=0;
    printf("2's: %s\n",ans);
}
