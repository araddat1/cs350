/* Andrew Allen Raddatz
 * CS 350 Spring 2012
 * Lab 8: SDC simulator */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Declarations
#define NBR_REGS 10
#define MEMORY_AMT 100
#define BUFFER_LENGTH 80

// For Sanity
    int manual = 0;     // automatic memory setting
    int debug = 0;      // automatic command execution

// Prototypes
    void readMemory(int *mem);
    void setMemory(int *mem);
    void dumpRegisters(int *regs);
    void dumpMemory(int *mem);
    int instruction_cycle(int *pc, int *ir, int *mem, int *regs);
    void printString(int addr, int *mem);

int main(void) {
    int *regs = malloc(NBR_REGS*sizeof(int));
    int *mem = malloc(MEMORY_AMT*sizeof(int));
    int *pcp = malloc(sizeof(int)), *irp = malloc(sizeof(int)); *pcp = 0;
    char *command = malloc(BUFFER_LENGTH*sizeof(char));

    if (manual){readMemory(mem);} else {setMemory(mem);}

    printf("\nBeginning execution:\n");
    char prompt[] = "> ";

    int done = 0;
    while (!done) {
        if (debug){
            printf("%s", prompt);
            scanf("%s",command);
            if (strcmp("q",command)==0) { return 0;}
            else if (strcmp(command,"\n") != 0) { printf("Unknown command; ignoring it.\n");}
        }
        done = instruction_cycle(pcp, irp, mem, regs);
    }
    printf("Halting.\n");
    dumpRegisters(regs);
    dumpMemory(mem);
}

void readMemory(int *mem) {
    int i;
    for (i=0; i<MEMORY_AMT; i++){mem[i]=0;}
    int location = 0;
    printf("Loc %02d: ", location );
    int valread;
    scanf("%d", &valread);
    while (valread <= 9999 && valread >= -9999) {
        mem[location]=valread;
        location++;
        printf("Loc %02d: ", location );
        scanf("%d", &valread);
    }
    printf("\nInitial value of memory:\n");
    dumpMemory(mem);
}

void setMemory(int *mem){
    int i;
    for (i=0; i<MEMORY_AMT; i++){mem[i]=0;}
    mem[  0] = 1090;    mem[  1] = 3091;    mem[  2] = 2093;    mem[  3] = 1193;    mem[  4] = 4100;
    mem[  5] = 5287;    mem[  6] = 6213;    mem[  7] = 1999;    mem[  8] = 3998;    mem[  9] = 1899;
    mem[ 10] = 6898;    mem[ 11] = 7022;    mem[ 22] = 1393;    mem[ 23] = 5605;    mem[ 24] = 8351;
    mem[ 30] =   65;    mem[ 31] =  110;    mem[ 32] =  100;    mem[ 33] =  114;    mem[ 34] =  101;    
    mem[ 35] =  119;    mem[ 36] =   32;    mem[ 37] =   65;    mem[ 38] =  108;    mem[ 39] =  108;    
    mem[ 40] =  101;    mem[ 41] =  110;    mem[ 42] =   32;    mem[ 43] =   82;    mem[ 44] =   97;    
    mem[ 45] =  100;    mem[ 46] =  100;    mem[ 47] =   97;    mem[ 48] =  116;    mem[ 49] =  122;    
    mem[ 51] = 5523;    mem[ 52] = 8170;    mem[ 53] = 1401;    mem[ 54] = 9230;    mem[ 55] = 5701;    
    mem[ 56] = 4700;    mem[ 57] = 2797;    mem[ 58] = 3697;    mem[ 59] = 8654;    mem[ 70] = 1400;    
    mem[ 71] = 9000;    mem[ 72] = 9100;    mem[ 90] = 1000;    mem[ 91] = 2000;    mem[ 92] = 3000;    
    mem[ 98] =  100;    mem[ 99] = 9999; 
}

void dumpRegisters(int *regs) {
    printf("\nRegisters:\n");
    int i, j;
    for (i = 0; i<2; i++){ for (j = 5*i; j<5+5*i; j++){ printf("R%d: %5d   ", j, regs[j]);} printf("\n");}
}

void dumpMemory(int *mem) {
    printf("\nMemory:\n");
    int i, j;
    for (i = 0; i<MEMORY_AMT; i+=10){ for (j = i; j<i+10; j++){ printf("Loc %2d: %5d   ", j, mem[j]);} printf("\n");}
}

void printString(int addr, int *mem){ while (mem[addr]!=0) printf("%c",mem[addr++]); printf("\n");}

int instruction_cycle(int *pcp, int *irp, int *mem, int *regs) {
    char cmd[5];
    if (*pcp < 0) { printf("Illegal pc = %d\n", *pcp); return 1;}

    *irp = mem[(*pcp)++];
    if (*pcp == 100){*pcp = 0;}
    sprintf(cmd,"%04d", *irp);
    int opcode = cmd[0]-48, reg = cmd[1]-48, addr = atoi(&cmd[2]);

    switch(opcode) {
        case 0: { /* HALT */ printf("HALT\n"); return 1;}
        case 1: { /* LD */ regs[reg] = mem[addr]; printf("LD   R%d <- M[%02d] = %d\n", reg, addr, regs[reg]); break;}
        case 2: { /* ST */ mem[addr]=regs[reg]; printf("ST   M[%02d] <- R%d = %d\n", addr, reg, regs[reg]); break;}
        case 3: { /* ADD */
                    if (regs[reg]+mem[addr]<=9999){ /* Standard Case */
                        printf("ADD  R%d <- R%d + M[%02d] = %d + %d = %d\n", reg, reg, addr, regs[reg], mem[addr], mem[addr] + regs[reg]);
                        regs[reg]+=mem[addr];
                        break;
                    } 
                    if (regs[reg]+mem[addr]>=9999){ // Rollover high
                        printf("ADD  R%d <- R%d + M[%02d] = %d + %d = %d\n", reg, reg, addr, regs[reg], mem[addr], regs[reg] + mem[addr] - 10000);
                        regs[reg] += mem[addr] - 10000;
                        break;
                    } 
                    else { // Rollover low
                        printf("ADD  R%d <- R%d + M[%02d] = %d + %d = %d\n", reg, reg, addr, regs[reg], mem[addr], regs[reg] + mem[addr] + 10000);
                        regs[reg] += mem[addr] + 10000;
                        break;
                    }
                }
        case 4: { /* NEG */ regs[reg] = -regs[reg]; printf("NEG  R%d <- -R%d  = %d\n", reg, reg, regs[reg]); break;}
        case 5: { /* LDM */ printf("LDM  R%d <- % 05d = %d\n", reg, addr,regs[reg] = addr); break;}
        case 6: { /* ADDM */
                    if (regs[reg]+addr<=9999){ /* Standard case */
                        printf("ADDM R%d <- R%d + % 05d = %d\n", reg,reg,addr, regs[reg] + addr);
                        regs[reg] += addr;
                        break;
                    }  
                    if (regs[reg]+addr>=9999){ // Rollover high
                        printf("ADDM R%d <- R%d + % 05d = %d\n", reg,reg,addr, regs[reg] + addr - 10000);
                        regs[reg] += addr - 10000;
                        break;
                    } 
                    else { // Rollover low
                        printf("ADDM R%d <- R%d + % 05d = %d\n", reg,reg,addr, regs[reg] + addr + 10000);
                        regs[reg] += addr + 10000;
                        break;
                    }
                }
        case 7: { /* BR */ printf("BR   %d\n", addr); *pcp = addr; break;}
        case 8: { /* BRP */ if (regs[reg]>0){ printf("BRP  %d\n", addr); *pcp=addr;} else { printf("BRP NO\n");} break;}
        case 9: { /* I/O */
                    switch (reg) {
                        case 0: { /* Read in and Print char */ 
                                    printf("I/O  Read char   Enter a char (and press return): ");
                                    char valReadIn;
                                    scanf("%c",&valReadIn);
                                    regs[0] = valReadIn;
                                    printf("R0 <- %d\n", valReadIn);
                                    scanf("%c",&valReadIn);
                                    break;
                                }
                        case 1: { /* Print char */ printf("I/O  1: Print char in R0 (= %d): %c\n", regs[0], (char) regs[0]); break;}
                        case 2: { /* Print string */ printf("I/O  2: Print String   "); printString(addr, mem); break;}
                        case 3: { /* Dump registers */ printf("I/O  3: Dump Register\n"); dumpRegisters(regs); break;}
                        case 4: { /* Dump memory */ printf("I/O  4: Dump Memory\n"); dumpMemory(mem); break;}
                        default:{ printf("Unknown I/O; skipped\n");}
                    }
                    break;
                }
        default: { printf("Bad opcode?"); break;}
    }
    return 0;
}
