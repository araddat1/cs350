/* Andrew Allen Raddatz
 * CS 350 Lab 3 Key
 * Feb 8, 2012
 * */

 /* Part 1
  * 1. Truth table for (x+y)*z+(x'+y)*z'
  *     x y z (x+y) (x+y)*z  x' (x'+y) z' (x'+y)*z'  (x+y)*z+(x'+y)*z'
  *     0 0 0   0      0     1    1    1     1              1
  *     0 0 1   0      0     1    1    0     0              0
  *     0 1 0   1      0     1    1    1     1              1
  *     0 1 1   1      1     1    1    0     0              1
  *     1 0 0   1      0     0    0    1     0              0
  *     1 0 1   1      1     0    0    0     0              1
  *     1 1 0   1      0     0    1    1     1              1
  *     1 1 1   1      1     0    1    0     0              1
  *
  * 2. Truth table for (u+v)(v'+w)+w'(z'+u')
  *     u v w z (u+v) (v'+w) w' (z'+u') (u+v)(v'+w)   w'(z'+u')  (u+v)(v'+w)+w'(z'+u')
  *     0 0 0 0   0     1    1     1        0            1                  1
  *     0 0 0 1   0     1    1     1        0            1                  1
  *     0 0 1 0   0     1    0     1        0            0                  0
  *     0 0 1 1   0     1    0     1        0            0                  0
  *     0 1 0 0   1     0    1     1        0            1                  1
  *     0 1 0 1   1     0    1     1        0            1                  1
  *     0 1 1 0   1     1    0     1        1            0                  1
  *     0 1 1 1   1     1    0     1        1            0                  1
  *     1 0 0 0   1     1    1     1        1            1                  1
  *     1 0 0 1   1     1    1     0        1            0                  1
  *     1 0 1 0   1     1    0     1        1            0                  1
  *     1 0 1 1   1     1    0     0        1            0                  1
  *     1 1 0 0   1     0    1     1        0            1                  1
  *     1 1 0 1   1     0    1     0        0            0                  0
  *     1 1 1 0   1     1    0     1        1            0                  1 
  *     1 1 1 1   1     1    0     0        1            0                  1
  *
  * 3. Truth table for ((x->y)->z)<->(x->(y->z))
  *     x y z   x->y    ((x->y)->z)     y->z        x->(y->z)       ((x->y)->z)<->(x->(y->z))
  *     0 0 0    1           1           1              0                       0
  *     0 0 1    1           1           0              1                       1
  *     0 1 0    0           1           1              0                       0
  *     0 1 1    0           0           1              0                       1
  *     1 0 0    1           1           1              1                       1
  *     1 0 1    1           1           0              1                       1
  *     1 1 0    1           1           1              1                       1
  *     1 1 1    1           1           1              1                       1
  *
  * 4. DNF
  *     (U'V'X'Y')+(U'V'XY')+(U'V'XY)+(U'VXY')+(UV'X'Y')+(UV'XY')+(UVX'Y)+(UVXY)
  */

#include<stdio.h>
#include<string.h>
#include<math.h>
#include<stdlib.h>

#define buffersize 100
#define range 31

long str2dec(char *str);
char * dec2bin(long dec);
char * bin2comp(char *str, int neg);
char * comp2hex(char *str);

int main(){
    int done = 0;

    while (!done){ 
        int neg = 0, i;
        char userinput[buffersize], *bitstring, *compstring, *hexstring;
        
        printf("Enter a number: ");
        scanf("%s",userinput);

<<<<<<< HEAD
        if (userinput[0] == '-'){ for (i = 0; i<strlen(userinput); i++){ userinput[i]=userinput[i+1];} neg = 1;}
=======
        if (strcmp(userinput,"0")==0) {done = 1; continue;}
        
        if (userinput[0] == '-'){
            for (i = 0; i<strlen(userinput); i++){ userinput[i]=userinput[i+1];}
            neg = 1;
        }
>>>>>>> bc83400714b589eb72f53deb87551d6bb1366d8d

        long val = str2dec(userinput);
        printf("decimal value: %li\n", val);
        if (val > pow(2,range)-1 || val < -pow(2,range)){printf("Out of Range!\n"); continue;}

        bitstring = dec2bin(val);

        compstring = bin2comp(bitstring, neg);
        printf("2's comp: %s\n",compstring);

        hexstring = comp2hex(compstring);
        if (neg){ printf("%X\n",-val);}
        else { printf("%X\n",val);}
        printf("Hex string: %s\n\n",hexstring);
    }
}

long str2dec(char *str){
    int len = strlen(str), i;
    long val = 0;
    for (i = len-1; i >= 0; i--){ val = val + (str[i]-48)*pow(10,len-1-i);}
    return val;
}

char * dec2bin(long dec){
    char *bin = malloc(buffersize);
    int i;
    for(i = range; i > -1; i--){
        if (dec >= pow(2,i)){
            strcat(bin, "1");
            dec = dec - pow(2,i);                   
        } else { strcat(bin, "0");}
    }
    while(bin[1]=='0'){ for (i = 0; i<strlen(bin); i++){ bin[i]=bin[i+1];}}
    return bin;
}

char * bin2comp(char *str, int neg){
    char *ans = malloc(buffersize);
    int len = strlen(str), i;
<<<<<<< HEAD
    if (!neg){ free(ans); return str;}
=======
    if (!neg){ 
        free(ans);
        return str;
    }
>>>>>>> bc83400714b589eb72f53deb87551d6bb1366d8d

    for (i = 0; i<len; i++){ if (str[i]=='1'){str[i]='0';} else {str[i]='1';}}

    char one[len], temp;
    for (i = 0; i<len-1; i++){ one[i] = 48;}
    one[len-1]=49;

    for (i = len-1; i>=0; i--){
        temp = one[i]+str[i];
        if (temp=='0'+'0'){ ans[i]='0';}
        if (temp=='0'+'1'){ ans[i]='1';}
        if (temp=='1'+'1'){ str[i-1]++; ans[i]='0';}
        if (temp=='1'+'2'){ str[i-1]++; ans[i]='1';}
    }
    
    ans[len]=0;
    if (strlen(ans)%4==1 && ans[1]=='1' ){ return &ans[1];} 
    else {return ans;}
<<<<<<< HEAD
=======
}

int poweroftwo(char *str){
    int i;
    if (str[0]!='1'){return 0;}
    for (i = 1; i<strlen(str); i++){ if (str[i]!='0'){return 0;}} 
    return 1;
>>>>>>> bc83400714b589eb72f53deb87551d6bb1366d8d
}

char * comp2hex(char *str){
    char *ans = malloc(buffersize);
    int len = strlen(str), i;

    while (strlen(str)%4!=0){ for (i=len+1; i>=0; i--){ str[i+1]=str[i];} }
    for (i = 0; i<len; i = i+4){
        char temp[5] = {str[i], str[i+1], str[i+2], str[i+3], 0};
        if (strcmp(temp,"0000")==0){strcat(ans,"0");}
        if (strcmp(temp,"0001")==0){strcat(ans,"1");}
        if (strcmp(temp,"0010")==0){strcat(ans,"2");}
        if (strcmp(temp,"0011")==0){strcat(ans,"3");}
        if (strcmp(temp,"0100")==0){strcat(ans,"4");}
        if (strcmp(temp,"0101")==0){strcat(ans,"5");}
        if (strcmp(temp,"0110")==0){strcat(ans,"6");}
        if (strcmp(temp,"0111")==0){strcat(ans,"7");}
        if (strcmp(temp,"1000")==0){strcat(ans,"8");}
        if (strcmp(temp,"1001")==0){strcat(ans,"9");}
        if (strcmp(temp,"1010")==0){strcat(ans,"A");}
        if (strcmp(temp,"1011")==0){strcat(ans,"B");}
        if (strcmp(temp,"1100")==0){strcat(ans,"C");}
        if (strcmp(temp,"1101")==0){strcat(ans,"D");}
        if (strcmp(temp,"1110")==0){strcat(ans,"E");}
        if (strcmp(temp,"1111")==0){strcat(ans,"F");}
    }
    return ans;
}
